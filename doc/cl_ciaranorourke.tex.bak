\documentclass[10pt,a4wide,sans,colorlinks,linkcolor=true]{moderncv}     
\usepackage[ddmmyyyy]{datetime}   
\moderncvstyle{classic}                            
\moderncvcolor{green}                              
\usepackage[utf8]{inputenc}                       
\usepackage[scale=0.75, top=1in, bottom=1in, left=1in, right=1in]{geometry}
\def\mysingleq#1{`#1'}


\name{Ciar\'an}{O' Rourke}
\title{ICHEC Cover Letter}
\address{109, The Ice Rink}{Reuben Square, Dolphins Barn}{Dublin 8}
\phone[mobile]{087 064 8438}                   
\email{orourkc8@tcd.ie}

\begin{document}

\hypersetup{urlcolor=gray}

\recipient{Kathryn Kozarits\\Research Recruitment\\Human Resources}{Quadrangle Building\\University Road\\Galway}
\date{\today}
\opening{Dear Ms. Kozarits,}
\closing{ \vspace{10pt} Yours sincerely, \\
\vspace{10pt} \underline{\includegraphics[width=0.3\linewidth]{../images/signature.eps}} \vspace{-30pt} }

\makelettertitle

I wish to express my sincere interest in applying for the position of Research Computational Scientist with ICHEC. 

I strongly feel that I would be an ideal candidate for this position, as I am currently fullfilling the duties of this role in my current position at ICHEC. My role involves active participation in a number of commercial consultancy projects in the Oil and Gas industry as well as my duties in user support and technical evaluation of project proposals as part of the National HPC Service. Prior to joining ICHEC, I received a (Distinction) M. Sc in High Performance Computing and have previously completed my undergraduate studies in Theoretical Physics. My background in physics has proved a valuable asset during my time with ICHEC, particularly aiding my performance of technical evaluations of project proposals in the field of astrophysics. 

I have strong programming skills in C and C++, as evidenced by my attainment of First Class Honour's in both related modules taken as part of the HPC course. I gained experience with both parallel computing through MPI and concurrent programming through OpenMP and Pthreads during my High-Performance Computing Master's, wherein I received a First Class Honour's in High-Performance Computing Software. Many of the ideas learned during my studies were incorporated into my thesis project on \mysingleq{\href{https://gitlab.com/CiaranORourke/LatticeQFT}{Parallel Implementation of Lattice Field Theory Simulations in Arbitrary Dimensions}}. During my Master's studies, I also gained exposure to GPGPU through the Introduction to CUDA module in which I obtained a First Class Honour's result. I possess a problem-solving ability as a natural aptitude; this has been shaped and developed throughout my studies, particularly during my Undergraduate studies of Theoretical Physics which holds creative problem solving as a key learning objective. Additionally, I have demonstrated a keen ability to analyse and form conclusions around raw data; most notably as part of my collaborative final year Physics Project titled \mysingleq{Energy Loss From Public Lighting and the Impact of New Technology} wherein ISS and VIRSS satellite data were combined with public lighting data to analyse trends in lamp types and to isolate commercial sources of light pollution.

Moreover, I am greatly excited at the prospect of continuing my work with ICHEC. The emphasis on training and development at ICHEC as well as the opportunity to work on commercial projects has allowed me to immensely improve both my HPC knowledge and my core programming skills. I feel that I have also benefitted in this regard from my colleagues, who are always eager to share ideas and best practices. My participation in active development environments in industry has given me exposure to modern revision control and continuous integration practices. A renewed role at ICHEC would allow me to continue learning at a fast rate and give me the chance to see the projects I have been working on through to their completion. 

I would be grateful for the opportunity to be called for interview. Please also refer to my CV for further information regarding my experience and skills and follow the hyperlink to my Master's project code above or in my CV for an indication of my programming level.
 
Sincere thanks for your consideration. I look forward to hearing from you soon.
 
\makeletterclosing

\end{document}














