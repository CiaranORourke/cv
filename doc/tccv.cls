\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{tccv}
              [2022/04/05 v1.0 Curriculum Vitae]

\LoadClass[10pt]{scrartcl}

\setcounter{secnumdepth}{-1}
\RequirePackage[hmargin=1.25cm,vmargin=1.25cm,twocolumn,columnsep=1.25cm]{geometry}
\RequirePackage{bookman,etoolbox,hyperref,marvosym,needspace,tabularx,xcolor}

\newcommand\ucwords[2][3]{%
    % Fails if not in LuaLaTeX
    \providecommand\directlua[1]{#2}%
    \directlua{%
	local minlen=tonumber("#1")
	local src="\luaescapestring{\unexpanded{#2}}"
	local dst={}
	for w in src:gmatch('[^\string\%s]+') do
	    if w:len() >= minlen then w = w:sub(1,1):upper()..w:sub(2) end
	    table.insert(dst, w)
	end
	tex.print(dst)}}

\pagestyle{empty}
\setlength\parindent{0pt}
\color[HTML]{303030} % Default foreground color
\definecolor{link}{HTML}{506060} % Hyperlinks
\hypersetup{colorlinks,breaklinks,urlcolor=link,linkcolor=link}
\setkomafont{disposition}{\color[HTML]{801010}}
\setkomafont{section}{\scshape\Large\mdseries}

\renewcommand\part[5]{%
    \twocolumn
	\vskip-\lastskip%
	{\usekomafont{part} #1} \\
    {\personal{#2}{#3}{#4}}
    }

% Override the \section command to capitalize every word and draw a rule under it.
\makeatletter
\let\old@section\section
\renewcommand\section[2][]{%
    \old@section[#1]{\ucwords{#2}}%
    \newdimen\raising%
    \raising=\dimexpr-0.7\baselineskip\relax%
    \vskip\raising\hrule height 0.4pt\vskip-\raising}
\makeatother

% Personal info box:
\newcommand\personal[4][]{%
    \needspace{0.5\textheight}%
    \newdimen\boxwidth%
    \boxwidth=\dimexpr\linewidth-2\fboxsep\relax%
    \colorbox[HTML]{FCFF8E}{%
    \begin{tabularx}{\boxwidth}{c|X}
    \Writinghand & {#2}\smallskip\\
    \Telefon     & {#3}\smallskip\\
    \Letter      & \href{mailto:#4}{#4}
    \ifstrempty{#1}{}{\smallskip\\ \Lightning & \href{http://#1}{#1}}
    \end{tabularx}}}

% \item{tagline}{title}{text}
\newenvironment{projectlist}{%
    \newcommand*\inskip{}
    \newcommand\citem[3]{%
	\inskip%
	{\Large\it ##2} \\
    {##1}
    {##3}
	\renewcommand\inskip{\bigskip}}}
    {}

% \item[optional: extra field]{years}{title}{notes}
\newenvironment{yearlist}{%
    \renewcommand\item[5][]{%
	{\sc ##3} & {\bfseries ##4} \\
    \ifstrempty{##1}{}{{} & {\sc ##1} \\}
    {\sc ##2} & {\it ##5}\medskip\\}
    \tabularx{\linewidth}{rX}}
    {\endtabularx}

\newenvironment{skillslist}{%
     \renewcommand\item[9]{%
    {\bfseries ##1} &
    {\it ##2
        \ifstrempty{##3}{}{$\bullet$ \it ##3}
        \ifstrempty{##4}{}{$\bullet$ \it ##4}
        \ifstrempty{##5}{}{$\bullet$ \it ##5}
        \ifstrempty{##6}{}{$\bullet$ \it ##6}
        \ifstrempty{##7}{}{$\bullet$ \it ##7}
        \ifstrempty{##8}{}{$\bullet$ \it ##8}
        \ifstrempty{##9}{}{$\bullet$ \it ##9}
    } \smallskip \\}
    \tabularx{\linewidth}{rX}}
    {\vspace{-5mm}\endtabularx}
    

% \item{fact}{description}
\newenvironment{factlist}{%
    \newdimen\unbaseline
    \unbaseline=\dimexpr-\baselinestretch\baselineskip\relax
    \renewcommand\item[2]{%
	\textsc{##1} & {\raggedright ##2\medskip\\}\\[\unbaseline]}
    \tabularx{\linewidth}{rX}}
    {\endtabularx}
